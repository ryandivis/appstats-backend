/**
 * Created by admin on 4/27/16.
 */
var gulp = require('gulp');
var child_process = require('child_process');

function runCommand(command) {
    return function (cb) {
        child_process.exec(command, function (err, stdout, stderr) {
            console.log(stdout);
            console.log(stderr);
            cb(err);
        });
    }
}

gulp.task('start-all', ['redis-start', 'start-mongo', 'start-app']);

gulp.task('start-background', ['redis-start', 'start-mongo']);

gulp.task('redis-start', runCommand('redis-server'));
gulp.task('start-mongo', runCommand('sudo mongod'));
gulp.task('stop-mongo', runCommand('mongo --eval "use admin; db.shutdownServer();"'));
gulp.task('start-app', runCommand('node app/app.js'));

