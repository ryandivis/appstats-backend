/**
 * Created by admin on 4/13/16.
 */
var express = require('express');
var kue = require('kue');
var queue = kue.createQueue();
var AppAnnie = require('./connectors/app-annie');
var Flurry = require('./connectors/flurry');
var Sample = require('./connectors/sample');
var SampleWorker = require('./workers/sample.worker');
var app = express();

app.get('/', function (req, res) {
    var sample = new Sample();
    if (!sample.error) {

    }
    res.send('Hello World!');
});

app.get('/annie', function (req, res) {
    var appAnnie = new AppAnnie('24a430ccdfd14599cb2248304b1015709dc3f187', 181881, 282769257, 'ratings', '2015-12-21', '2016-04-20');
    appAnnie.get().then(function (data) {
        res.send(data);
    });
});

app.get('/flurry', function (req, res) {
    var flurry = new Flurry('JM23D3PNZM9VAT2RS53R', 'NewUsers', 0, 'metric', '2015-12-21', '2016-04-20');
    flurry
        .get()
        .then(function (data) {
            res.send(data);
        })
        .catch(function(){

        });
});

app.get('/queue/save', function (req, res) {

    var job = queue
        .create('sample', {
            apiKey: 181881,
            appId: 181881,
            productId: 282769257,
            metric: 'revenue',
            startDate: '2015-12-21',
            endDate: '2016-04-20'
        })
        .save(function (err) {
            if (!err) {
                res.send('Job:' + job.id);
            } else {
                res.send(err);
            }

        });
});

app.get('/queue/work', function (req, res) {

    var worker = new SampleWorker();

    var data = worker.work();

    res.send('worker worked' + data);
});

app.get('/queue/sample', function (req, res) {

    var sample = new Sample(123, 1881118, 2, 'revenue', '2016-04-01', '2016-04-27');

    sample
        .get()
        .then(function(data) {
            res.send(data);
        })
        .catch(function(error) {
            res.send('error', error);
        })
});

app.listen(3000, function () {
    console.log('listening on port 3000!');
});