//https://github.com/rschmukler/agenda

var Agenda = require('agenda');
var ClassExtend = require('class-extend');

var FlurryWorker = require('./workers/flurry.worker');
var AppAnnieWorker = require('./workers/app-annie-worker');

var SampleAgenda = module.exports = ClassExtend.extend({

    schedule: function () {


    	// for intervals see https://github.com/rschmukler/human-interval
		var schedule = [
			{interval: '15 minutes', name: 'flurry worker', action: FlurryWorker},
			{interval: '15 minutes', name: 'app annie worker', action: AppAnnieWorker}
		]

		var agenda = new Agenda({db: {address: 'localhost:27017/appstats'}});

		schedule.forEach(function(worker){
			
			agenda.define(worker.name, function(job, done) {
				console.log('running ' + worker.name);
				var action = new worker.action();
				action.work();
				done();
			});

		});

		agenda.on('ready', function(){
			schedule.forEach(function(worker){
				agenda.every(worker.interval, worker.name);
			});
			agenda.start();
		});

		

    }

});