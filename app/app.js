/**
 * Created by admin on 4/13/16.
 */
var express = require('express');
var AppAnnie = require('./connectors/app-annie');
var Flurry = require('./connectors/flurry');
var Sample = require('./connectors/sample');
var app = express();
var bodyParser = require('body-parser');
var Agenda = require('agenda');
var mongoose = require('mongoose');
var S = require('string');
var bunyan = require('bunyan');
var moment = require('moment');
var mongoConnectionString = "mongodb://appAdmin:Welcome99@40.114.83.105/appstats";

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var logger = bunyan.createLogger({
    name: 'appstats',
    streams: [{
        path: 'logs/appstats-' + moment().format('MM-DD-YYYY') + '.log'
    }]
});

app.get('/', function (req, res) {
    var sample = new Sample();
    if (!sample.error) {

    }
    res.send('Hello World!');
});

function getMonthDateRange(year, month) {
    // month in moment is 0 based, so 9 is actually october, subtract 1 to compensate
    // array is 'year', 'month', 'day', etc
    var startDate = moment([year, month]);

    // Clone the value before .endOf()
    var endDate = moment(startDate).endOf('month');

    // make sure to call toDate() for plain JavaScript date type
    return { start: startDate.toDate(), end: endDate.toDate() };
}

app.post('/start', function (req, res) {

    var agenda = new Agenda({ db: { address: mongoConnectionString } });

    var company = req.body.company;
    var app = req.body.app;

    if (!company || !app) {
        var err = 'You must pass in a company and an app';
        logger.error({ error: err, method: 'start', body: req.body });
        res.send(err);
        return;
    }

    var jobs;

    var flurryMetrics = ['NewUsers', 'RetainedUsers', 'MedianSessionLength'];

    var currMonth = moment().month();
    var currYear = moment().year();

    var dates = [];

    //initial setup stuff
    for (var i = 0; i < 13; i++) {

        var date = getMonthDateRange(currYear, currMonth - i);

        dates.push(date);

        var job = queue
            .create('appAnnie', {
                apiKey: app.appAnnie.apiKey,
                appId: app.appAnnie.accountId,
                productId: app.appAnnie.productId,
                metric: 'revenue',
                startDate: date.startDate.format('YYYY-MM-DD'),
                endDate: date.endDate.format('YYYY-MM-DD')
            })
            .save(function (err) {
                if (err) {
                    res.send(err);
                }
                jobs.push(job.id);
            });

        flurryMetrics.forEach(function (metric) {
            var job = queue
                .create('flurry', {
                    apiKey: app.flurry.apiKey,
                    appId: '',
                    productId: '',
                    metric: metric,
                    startDate: date.startDate.format('YYYY-MM-DD'),
                    endDate: date.endDate.format('YYYY-MM-DD')
                })
                .save(function (err) {
                    if (err) {
                        res.send(err);
                    }
                    jobs.push(job.id);
                });
        });


    }

    console.log(jobs);

    //fill the queue with items for the last 12 months for appAnnie
    //fill the queue with items for the last 12 months for flurry

    res.send('hello: ' + company);

});

app.get('/run', function (req, res) {

});

// app.get('/annie', function (req, res) {
//     var appAnnie = new AppAnnie('24a430ccdfd14599cb2248304b1015709dc3f187', 181881, 282769257, 'revenue', '2015-12-21', '2016-04-20');
//     appAnnie.get().then(function (data) {
//         res.send(data);
//     }).catch(function(err) {
//       res.send(err);
//     });
// });

// app.get('/flurry', function (req, res) {
//     var flurry = new Flurry('KVRVPGQK9FT835C5GY9H', 'NewUsers', 0, 'metric', '2016-04-15', '2016-04-20');
//     flurry
//         .get()
//         .then(function (data) {
//             res.send(data);
//         })
//         .catch(function(err){
//           res.send(err);
//         });
// });

// app.get('/queue/save', function (req, res) {

// var job = queue
//     .create('sample', {
//         apiKey: 181881,
//         appId: 181881,
//         productId: 282769257,
//         metric: 'revenue',
//         startDate: '2015-12-21',
//         endDate: '2016-04-20'
//     })
//     .save(function (err) {
//         if (!err) {
//             res.send('Job:' + job.id);
//         } else {
//             res.send(err);
//         }

//     });
// });

// app.get('/queue/work', function (req, res) {

//     var worker = new SampleWorker();

//     var data = worker.work();

//     res.send('worker worked' + data);
// });

// app.get('/queue/sample', function (req, res) {

//     var sample = new Sample(123, 1881118, 2, 'revenue', '2016-04-01', '2016-04-27');

//     sample
//         .get()
//         .then(function(data) {
//             res.send(data);
//         })
//         .catch(function(error) {
//             res.send('error', error);
//         });
// });
// app.get('/queue/flurry', function (req, res) {

//     var flurry = new Flurry('KVRVPGQK9FT835C5GY9H', 'NewUsers', 0, 'metric', '2016-04-15', '2016-04-20');
//     flurry
//         .get()
//         .then(function (data) {
//             res.send(data);
//         })
//         .catch(function(err){
//           res.send(err);
//         });
// });

app.listen(3000, function () {
    console.log('listening on port 3000!');
});