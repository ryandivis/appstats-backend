/**
 * Created by admin on 4/18/16.
 */
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var UnifiedDataModel = new Schema({
    date: Date,
    source: String,
    productId: String,
    country: String,
    type: String,
    units: Number
});

var UnifiedModel = mongoose.model('data', UnifiedDataModel);

module.exports = UnifiedModel;