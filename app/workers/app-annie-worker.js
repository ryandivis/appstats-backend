/**
 * Created by admin on 4/27/16.
 */
var kue = require('kue');
var queue = kue.createQueue();
var Promise = require('bluebird');
var mongoose = require('mongoose');
var moment = require('moment');
var bunyan = require('bunyan');
var UnifiedModel = require('../models/udm');
var AppAnnie = require('../connectors/app-annie');
var processQueue = Promise.promisify(queue.process);
var BaseWorker = require('./base.worker');

var AppAnnieWorker = module.exports = BaseWorker.extend({

    work: function () {
        var self = this;
        queue.process('flurry', function (job, done) {

            var appAnnie = new AppAnnie(job.data.apiKey, job.data.appId, job.data.productId, job.data.metric, job.data.startDate, job.data.endDate);

            appAnnie
                .get()
                .then(function (data) {
                    self.log.info(data);
                    //save the data to the database
                    data['ratings'].forEach(function(item) {
                        var instance = new UnifiedModel(item);
                        instance.save(function (err) {
                            if (err) {
                                self.log.error(err);
                            }
                        });
                    });

                    data['downloads'].forEach(function(item) {
                        var instance = new UnifiedModel(item);
                        instance.save(function (err) {
                            if (err) {
                                self.log.error(err);
                            }
                        });
                    });

                    data['revenue'].forEach(function(item) {
                        var instance = new UnifiedModel(item);
                        instance.save(function (err) {
                            if (err) {
                                self.log.error(err);
                            }
                        });
                    });
                    done(null, data);
                })
                .catch(function (error) {
                    //log the error
                    self.log.error(error);
                    done(new Error(error));
                });
        });
    }

});