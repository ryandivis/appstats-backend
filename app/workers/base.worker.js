/**
 * Created by admin on 4/27/16.
 */
var ClassExtend = require('class-extend');
var kue = require('kue');
var queue = kue.createQueue();
var Promise = require('bluebird');
var mongoose = require('mongoose');
var moment = require('moment');
var bunyan = require('bunyan');
var UDM = require('../models/udm');
var Sample = require('../connectors/sample');
var processQueue = Promise.promisify(queue.process);

mongoose.connect('mongodb://localhost/appstats');

var BaseWorker = module.exports = ClassExtend.extend({

    constructor: function () {

        var self = this;

        self.log = bunyan.createLogger({
            name: 'appstats',
            streams: [{
                path: 'logs/appstats-' + moment().format('MM-DD-YYYY') + '.log'
            }]
        });

        queue
            .on('error', function (err) {
                self.log.error(err);
            })
            .on('job enqueue', function (id, type) {
                self.log.info('Job %s got queued of type %s', id, type);

            })
            .on('job complete', function (id, result) {
                kue.Job.get(id, function (err, job) {
                    if (err) return;
                    job.remove(function (err) {
                        if (err) throw err;
                        self.log.info('removed completed job #%d', job.id);
                    });
                });
            });

    }

});