/**
 * Created by admin on 4/27/16.
 */
var kue = require('kue');
var queue = kue.createQueue();
var Promise = require('bluebird');
var mongoose = require('mongoose');
var moment = require('moment');
var bunyan = require('bunyan');
var UnifiedModel = require('../models/udm');
var Flurry = require('../connectors/flurry');
var processQueue = Promise.promisify(queue.process);
var BaseWorker = require('./base.worker');

var FlurryWorker = module.exports = BaseWorker.extend({

    work: function () {
        var self = this;
        queue.process('flurry', function (job, done) {

            var flurry = new Flurry(job.data.apiKey, job.data.appId, job.data.productId, job.data.metric, job.data.startDate, job.data.endDate);

            flurry
                .get()
                .then(function (data) {
                    self.log.info(data);
                    //save the data to the database
                    data['metrics'].forEach(function(item) {
                        var instance = new UnifiedModel(item);
                        instance.save(function (err) {
                            if (err) {
                                self.log.error(err);
                            }
                        });
                    });

                    done(null, data);
                })
                .catch(function (error) {
                    //log the error
                    self.log.error(error);
                    done(new Error(error));
                });
        });
    }

});