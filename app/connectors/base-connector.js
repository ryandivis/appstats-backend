/**
 * Created by admin on 4/19/16.
 */
var ClassExtend = require('class-extend');
var Promise = require('bluebird');
var bunyan = require('bunyan');
var moment = require('moment');
var UDM = require('../models/udm');

var Base = module.exports = ClassExtend.extend({

    constructor: function(apiKey, accountId, productId, type, startDate, endDate) {

        //setup logging
        this.log = bunyan.createLogger({
            name: 'appstats',
            streams: [{
                path: 'logs/appstats-' + moment().format('MM-DD-YYYY') + '.log'
            }]
        });

        this.apiKey = apiKey;
        this.accountId = accountId;
        this.productId = productId;
        this.type = type;
        this.startDate = startDate;
        this.endDate = endDate;

        try {
            this.validate();
        } catch (err) {
            this.error = err;
            this.log.error({ error: err });
        }

    },

    acceptableTypes : ['downloads', 'revenue', 'ratings', 'metric'],

    error: null,

    validate : function() {

        //type must be an acceptable type for this connector
        if (this.acceptableTypes.indexOf(this.type) === -1) {
            throw ('type: ' + this.type + 'was not in the acceptable types for this connector');
        }

        //start date must be before the endDate and both must be parseable as dates
        if(!moment(this.startDate).isBefore(this.endDate)) {
            throw ('start date was before endDate');
        }

    },

    //get: function() {
    //
    //}

});