/**
 * Created by admin on 4/19/16.
 */
'use strict';

var Base = require('./base-connector');
var moment = require('moment');
var Promise = require('bluebird');
var request = Promise.promisifyAll(require('request'));
var UDM = require('../models/udm');

var downloadsData = [
    {
        date: moment().format('MM-DD-YYYY'),
        source: 'sample',
        sourceId: 'abc123',
        country: 'US',
        type: 'downloads',
        units: 100
    },
    {
        date: moment().format('MM-DD-YYYY'),
        source: 'sample',
        sourceId: 'abc123',
        country: 'CA',
        type: 'downloads',
        units: 99
    }
];

var revenueData = [
    {
        date: moment().format('MM-DD-YYYY'),
        source: 'sample',
        sourceId: 'abc123',
        country: 'US',
        type: 'revenue',
        units: 25
    },
    {
        date: moment().format('MM-DD-YYYY'),
        source: 'sample',
        sourceId: 'abc123',
        country: 'CA',
        type: 'revenue',
        units: 14
    }
];

var Sample = Base.extend({
    get: function () {
        var self = this;
        var promise = new Promise(function (resolve, reject) {
            resolve({
                'downloads': downloadsData,
                'revenue' : revenueData
            })
        });

        return promise;
    },
    getDownloads: function () {
        return request.get('http://www.google.com');
    },
    foobar: function () {
        return 'foobar';
    }
});

module.exports = Sample;