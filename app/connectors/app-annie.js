/**
 * Created by admin on 4/18/16.
 */
 'use strict';
/*
 GET PRODUCTS
 "method": "GET",
 "url": "https://api.appannie.com/v1.2/accounts/{account_id=181881}/products?page_index={page_index=0}",
 "headers": {
 "Authorization": "bearer 24a430ccdfd14599cb2248304b1015709dc3f187"
 }

 GET SALES DATA
 "method": "GET",
 "url": "https://api.appannie.com/v1.2/accounts/{account_id}/products/{product_id}/sales?break_down=date+country&start_date={start_date}&end_date={end_date}",
 "headers": {
 "Authorization": "bearer 24a430ccdfd14599cb2248304b1015709dc3f187"
 }

 GET RATINGS DATA
 "method": "GET",
 "url": "https://api.appannie.com/v1.2/apps/{market}/app/{product_id}/ratings?page_index={page_index=0}",
 "headers": {
 "Authorization": "bearer 24a430ccdfd14599cb2248304b1015709dc3f187"
 }
 */
var Base = require('./base-connector');
var moment = require('moment');
var Promise = require('bluebird');
var request = Promise.promisifyAll(require('request'));
var UDM = require('../models/udm.js');
var _ = require('lodash');

var annieUrl = 'https://api.appannie.com/v1.2/';


var AppAnnie = Base.extend({

    get: function() {
        var that = this;
        var promise = new Promise(function (resolve, reject) {
            switch(that.type) {
                case 'revenue':
                    //console.log('this');
                    return that.getRevenue(resolve, reject);
                case 'ratings':
                    //console.log('this2');
                    return that.getRatings(resolve, reject);

            }
        });
        return promise;
    },

    getRatings: function(resolve, reject) {
        var self = this;
        request.getAsync({
            url: annieUrl + 'apps/ios/app/' + this.productId + '/ratings?page_index=0',
            headers: {
                Authorization: 'bearer ' + this.apiKey
            }
        }).then(function(res) {
            if(res.statusCode !== 200) {
                self.log.error({error: res.body});
                return reject(res.body);
            } else {
                var ratingsArray = [];
                var resData = JSON.parse(res.body);
                _.forEach(resData.ratings, function(rating) {
                    var data = {
                            date: moment(),
                            source: 'AppAnnie',
                            productId: self.productId,
                            country: rating.country,
                            type: 'ratings',
                            units: rating.current_ratings.average
                    };
                    ratingsArray.push(new UDM(data));
                });

                return resolve({ratings:ratingsArray});
            }
        }).catch(function(err){
            self.log.error({error: err});
            return reject({text:'Error with results'}, err.body);
        });
    },

    getRevenue: function(resolve, reject) {
        var self = this;
        request.getAsync({
            url: annieUrl + 'accounts/' + this.accountId + '/products/' + this.productId + '/sales?break_down=date+country&start_date=' + this.startDate + '&end_date=' + this.endDate,
            headers: {
                Authorization: 'bearer ' + this.apiKey
            }
        }).then(function(res) {
            if(res.statusCode !== 200) {
                self.log.error({error: res.body});
                return reject({text:'Error with results'}, res.body);
            } else {
                var revenueArray = [];
                var downloadsArray = [];
                var resData = JSON.parse(res.body);
                _.forEach(resData.sales_list, function(sale) {
                    var downloadData = {
                        date: sale.date,
                        source: 'AppAnnie',
                        productId: self.productId,
                        country: sale.country,
                        type: 'downloads',
                        units: sale.units.product.downloads
                    };
                    var revenueData = {
                        date: sale.date,
                        source: 'AppAnnie',
                        productId: self.productId,
                        country: sale.country,
                        type: 'revenue',
                        units: sale.revenue.product.downloads
                    };
                    revenueArray.push(new UDM(revenueData));
                    downloadsArray.push(new UDM(downloadData));
                });
                return resolve({revenue:revenueArray, downloads:downloadsArray});
            }
        }).catch(function(err){
            self.log.error({error: err});
            return reject({text:'Error with results'}, err.body);
        });
    }

});

module.exports = AppAnnie;