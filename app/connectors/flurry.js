/**
 * Created by admin on 4/18/16.
 */
 'use strict';
/*
GET APPLICATIONS
 "method": "GET",
 "url": "http://api.flurry.com/appInfo/getAllApplications?apiAccessCode=5J9MCSWB3G3R56Y8P659"

GET SINGULAR APPLICATION
 "method": "GET",
 "url": "http://api.flurry.com/appInfo/getApplication?apiAccessCode=5J9MCSWB3G3R56Y8P659&apiKey={api_key}"

GET APPLICATION METRIC
 "method": "GET",
 "url": "http://api.flurry.com/appMetrics/{metric}?apiAccessCode=5J9MCSWB3G3R56Y8P659&apiKey={api_key}&startDate={start_date}&endDate={end_date}&country=all"
 */

var Base = require('./base-connector');
var Promise = require('bluebird');
var request = Promise.promisifyAll(require('request'));
var UDM = require('../models/udm.js');
var _ = require('lodash');

var flurryUrl = 'http://api.flurry.com/';

var Flurry = Base.extend({
  
   acceptableTypes : ['NewUsers', 'RetainedUsers', 'MedianSessionLength'],

  get: function() {
    var that = this;
    var promise = new Promise(function(resolve, reject) {
      switch(that.type) {
        case 'metric':
          return that.getMetric(resolve, reject);
      }
    });
    return promise;
  },
  
  getMetric: function(resolve, reject) {
    var self = this;
    request.getAsync({
      url: flurryUrl + 'appMetrics/' + this.accountId + '?apiAccessCode=5J9MCSWB3G3R56Y8P659&apiKey=' + this.apiKey + '&startDate=' + this.startDate + '&endDate=' + this.endDate + '&country=all'
    }).then(function(res) {
      if(res.statusCode !== 200) {
          self.log.error({error: res.body});
          return reject(res.body);
      } else {
          var resData = JSON.parse(res.body);
          var metricArray = [];

          _.forEach(resData.country, function(country) {
            var userData = {
              country: country['@country'],
              source: 'flurry',
              productId: self.productId,
              type: 'metric'
            };
            _.forEach(country.day, function(day) {
              userData.date = day['@date'];
              userData.units = day['@value'];

              metricArray.push(new UDM(userData));
            });
          });
          self.log.info({message: 'success'});
          return resolve({metrics:metricArray});
      }
    }).catch(function(err){
      self.log.error({error: err});
      return reject({text:'Error with results'}, err.body);
    });
  }

});

module.exports = Flurry;