# README #

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

Code style: https://github.com/ryandivis/javascript-standards

This is the repository for SolutionStream's domo application called appstats.

The logging functionality is contained at: http://otakudaro.com/

Login: solutionstream
Password: Welcome99

### How do I get set up? ###

Run npm install.

#### Database Setup ####

1. Install mongodb - brew update and then brew install mongodb
2. Create the data directory: mkdir -p /data/db
3. Set permissions for the data directory.
   Before running mongod for the first time, ensure that the user account running mongod has read and write permissions for the directory.
4. Open a new terminal window and run 'mongod' - this will run the mongo server in the background
5. Open another terminal window and run 'mongo' - this will open the mongo shell for you to use
6. type 'use appstats' - this will create the appstats database for you to work in

### Testing ###

#### Setup ####

We are using mocha and chai for our testing systems.

Mocha can be installed with: npm install mocha -g

Chai will be installed by a regular npm install

#### Running Unit Tests ####

Unit tests can be run by navigating to the appstats folder in the terminal. You then run mocha --recursive 
to run all tests located in the test folder

### Running Filebeat ###

Open your terminal. Navigate to the filebeat folder. Run:
sudo ./filebeat -e -c filebeat.yml -d "publish"

### Running the Queue Worker ###

1. Install redis - The easiest way on a mac is to first install homebrew (http://brew.sh/) and then install redis with 'brew install redis'
2. Install kue - npm install kue
3. Install agenda - npm install agenda
4. Install mongoose - npm install mongoose


### Default App Settings ###
#### App Annie ####
account_id = 181881

#### Product: Todo (Todo Cloud 8) ####
app annie product id = 408975584
flurry apikey = apiKey = KVRVPGQK9FT835C5GY9H

#### Product: Todo 8 ####
app annie product id = 708423616
flurry apikey = apiKey = TF3CD46Q3S37F9JHS9BP


### Aggregator ###

db.datas.aggregate(
	[
		{
			$project: {
				date: 1,
				country: 1,
				sourceId: 1,
				downloads: {
					$cond: {
						if: {
							$eq: ["$type", "downloads"]
						},
						then: "$units",
						else: 0
					}
				},
				revenue: {
					$cond: {
						if: {
							$eq: ["$type", "revenue"]
						},
						then: "$units",
						else: 0
					}
				}
			} 
		},
		{
			$group: {
				"_id" : {
					"date": "$date",
					"country" : "$country",
					"sourceId" : "$sourceId"
				},
				downloads: {
					$sum: "$downloads"
				},
				revenue: {
					$sum: "$revenue"
				}
			}
		}
	]
);