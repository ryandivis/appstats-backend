/**
 * Created by admin on 4/19/16.
 */
var chai = require('chai');
var expect = chai.expect;
var sinon = require('sinon');
var sinonBluebird = require('sinon-bluebird');
var request = require('request');

var Sample = require('./../../app/connectors/sample');

var apiKey = 'samplekey1231';
var accountId = 1;
var productId = 2;
var type = 'downloads';
var startDate = '2016-01-01';
var endDate = '2016-02-01';

var mockReturn = {
    statusCode: 200,
    body: '{ salesData: []}'
};

describe('Sample Connector', function() {

    before(function() {

    });

    after(function() {

    });

    it('should setup the initial object', function() {

        var sample = new Sample(apiKey, accountId, productId, type, startDate, endDate);

        expect(sample.error).to.equal(null);

        expect(sample.accountId).to.equal(accountId);

        expect(sample.productId).to.equal(productId);

        expect(sample.type).to.equal(type);

        expect(sample.startDate).to.equal(startDate);

        expect(sample.endDate).to.equal(endDate);

    });

    it('should handle errors if the type is not correct', function() {

        var sample = new Sample(apiKey, accountId, productId, 'bongo', startDate, endDate);

        expect(sample.error).to.equal('type was not in the acceptable types for this connector');

    });

    it('should be an example of unit testing a resolved promise', function(done){

        sinon.stub(request, 'get').resolves(mockReturn);

        var sample = new Sample(apiKey, accountId, productId, type, startDate, endDate);

        sample
            .getDownloads()
            .then(function(value) {
                expect(value).to.equal('hello world!');
                done();
            });

    });

    it('should be an example of unit testing a rejected promise', function(done){

        request.get.restore();

        sinon.stub(request, 'get').rejects('error');

        var sample = new Sample(apiKey, accountId, productId, type, startDate, endDate);

        sample
            .getDownloads()
            .catch(function(value) {
                expect(value).to.equal('error');
                done();
            });

    });

});